let product = {};

Object.defineProperties(product, {
  name: {
    value: "Smartphone",
  },
  price: {
    value: 1000,
  },
  discount: {
    value: 0.2,
  },
  netPrice: {
    get: function () {
      return this.price * this.discount;
    },
  },
});
console.log("Price of " + product.name + " is " + product.netPrice);

let person = {
  greeting: function () {
    return person.name, person.value;
  },
};

Object.defineProperties(person, {
  name: {
    value: prompt("Enter your name"),
  },
  age: {
    value: prompt("Enter your age"),
  },
});
alert("Hello, I'm " + person.name + " and I'm " + person.age + " years old");
